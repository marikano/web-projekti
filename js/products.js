var request = new XMLHttpRequest();
request.open('GET', 'data/products.json', false);
request.send(null);
var data = JSON.parse(request.responseText);
console.log(data);

var products = data.products;
var i = 0;
var k = 0;

//Upload first product
document.getElementById('showProduct').setAttribute("src", products[i].productimg);
document.getElementById('price').innerHTML = products[i].price;
document.getElementById('productname').innerHTML = products[i].productname;

//Upload the next or previous product
function slide(x){
  this.i = (i+x) % 3;
  document.getElementById('showProduct').setAttribute("src", products[i].productimg);
  document.getElementById('price').innerHTML = products[i].price;
  document.getElementById('productname').innerHTML = products[i].productname;
}

//Size selector
function size(){
  return document.getElementById("selector").value;
}

//Add to the cart message
function message(x){
  var message = 'Product '+products[i].productname+' was added to the shopping cart in size '+size()+'.';
  this.k = k+1;
  document.getElementById('shoppingcart').innerHTML = 'Ostoskori ('+k+')';
  return alert(message);
}